%define top_elem 0

%macro colon 2
	%%beginning:
	dq top_elem
	dq %2
	db %1, 0
	%define top_elem %%beginning
%endmacro
