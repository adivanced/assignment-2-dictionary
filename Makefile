all: lib.o main.o main

main.o: main.asm dict.asm words.inc colon.inc 
	nasm -f elf64 -g -F dwarf main.asm -o main.o

lib.o: lib.asm 
	nasm -f elf64 -g -F dwarf lib.asm -o lib.o

main: lib.o main.o 
	ld main.o lib.o -o main
