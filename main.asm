%include "lib.inc"

global _start

section .data
	%include "words.inc"
	not_found_msg db "String not found!", 0

section .text
	_start:
		sub rsp, 256

		xor rax, rax
		xor rdi, rdi
		mov rsi, rsp
		mov rdx, 255
		syscall
		mov byte [rsp+rax], 0

		call print_newline

		mov rdi, rsp
		mov rsi, top_elem
		call find_word

		or rax, rax
		jz not_found
		lea rdi, [rax+8]
		mov rdi, qword [rdi]
		call print_string
		call print_newline
		xor rdi, rdi
		call exit

		not_found:
		mov rax, 1
		mov rdi, 2
		mov rsi, not_found_msg
		mov rdx, 17
		syscall

		call print_newline

		mov rdi, 1
		call exit

		%include "dict.asm"


		
